import { Row, Col } from 'react-bootstrap';
import {Link, Outlet, Navigate} from 'react-router-dom';

export default function PageNotFound (){
  return (
    <>
    <h1>404 - Page Not Found</h1>
        <p>The requested page not found. Go to <Link to ="/">Homepage</Link> </p>
        </>

/*    // <Row>
    //   <Col className="ms-auto">
    //   <
    //   <Col/>
    // <Row/>*/

    )
};